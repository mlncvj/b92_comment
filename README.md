# B92 news portal - article publishing and comments section analysis

Over half a million news articles and over 5 million comments have been scraped from the Serbian news portal B92 (https://www.b92.net).
The article dataset contains information such as the title, the time and date of publishing and comment and the comment votes counts for each article.
The comment dataset contains information on the article each comment was left on, the time and date of publishing, the number of positive and negative votes,
the author and the comment content.
This page presents an overview of the analysis of the numbers and patterns of articles published and the number of readers' comments left on each article,
as well as votes these comments were given by readers.
Both datasets cover the time period between January 2014 and April 2019.

NOTE: This is a work in progress, and further analyses will be added in the future.

## Articles published

### 1) By category

Top three categories are:

 - "info" - politics, general news
 - "sport" - sports news
 - "biz" - business and economy
 
contributing to over 80% of all articles published. The other categories are:

 - "zivot" - lifestyle
 - "kultura" - culture
 - "tehnopolis" - technology
 - "automobili" - cars
 - "zdravlje" - health
 - "putovanja" - travel
 - "bbc" - news by BBC in Serbian (category created in March 2018)

In general, a slight downward trend in the number of published articles can be noticed across all categories.

Total number of published articles over the analysed time period:

 category    | articles | %
:----------- | :-----  | :----
 info        | 189747   | 37.9
 sport       | 127651   | 25.5
 biz         |  85352   | 17.1
 zivot       |  31988   | 6.4
 kultura     |  18741   | 3.7
 tehnopolis  |  13023   | 2.6
 automobili  |  10609   | 2.1
 zdravlje    |  10156   | 2.0
 putovanja   |   9901   | 2.0
 bbc         |   3417   | 0.7


![Articles published by section](https://bitbucket.org/mlncvj/b92_comment/raw/05e3057da32e8f427f9b0fbd997edfd1a63b5d00/1.png)

### 2) By day of week and time of day

Most articles are published during working days, between 8am and 6pm. Nighttime (1am to 7am) is almost exclusively reserved for breaking news in sports,
such as events happening in different time zones.


![Weekly heatmap](https://bitbucket.org/mlncvj/b92_comment/raw/f11a3e495674a9fd850961c0b75242c351c39def/2.png)


## Comments

### 1) By category

The top three categories regarding the overall number of comments are the same as in the case of the number of articles, with the sports category occasionally
overtaking the "info" category from 2016 onwards.

Another interesting thing to note is the sudden drop in the number of comments around *March 2016* in the *info* and *biz* categories.
As this does not correspond to any significant reduction in the number of articles published in these categories, and the remaining "hot" category (sports) does not
exhibit such a drop, one possible explanation is that the moderation procedures in these two categories have changed.
As a result, the number of published comments in these two categories has approximately halved in one month.

The gradual increase leading up to March 2014 can be attributed to the parliamentary elections held in Serbia in that month. The other two elections held in Serbia
during the time frame analysed both happened after the noted significant decrease in the number of comment published (April 2016 and April 2017), so their effects on the
number of comments is not easily recognisable.

![Comments by category](https://bitbucket.org/mlncvj/b92_comment/raw/2f33e40617886376a3bd4305e07b774574f14b9c/3.png)

### 2) Per article

Looking at the number of comments per article for different categories it can be concluded that articles in all sections apart from *cars*, *lifestyle* and *bbc* mostly
generate between 10 and 20 comments, with the overall average of the number of comments per article equal to *10.1*.

The described difference in the number of comments in the *info* and *biz* categories pre and post March 2016 can be viewed on a *comment-per-article* basis:

 category    | pre March 2016 | post March 2016 | change \[%\]
:----------- | :------- | :-------- | :---------
 info        | 15.2     | 7.9       | -48
 biz         | 14.4     | 9.4       | -35


![Comments by article](https://bitbucket.org/mlncvj/b92_comment/raw/540e5205fe1853b9bcb9547317036a2998d7b1f4/4.png)

### 3) By time article published

Time of publishing does not seem to influence the number of comments an article will generate, but there are a couple of interesting "anomalies" - i.e. early
Monday and Sunday morning articles seem to "explode" the comment as a rule. This is again because of the sporting events happening in different time zones,
in particular tennis tournaments which most of the time have semi-finals and finals matches played on Saturday and Sunday.

![Comments heatmap by article time](https://bitbucket.org/mlncvj/b92_comment/raw/93778bdadb8688c5ce1ef7f7a86d2115ad5dc73d/5.png)

In the top 17 most commented articles published between 2am and 6am on Sunday or Monday, only one is not related to sports, and the top three are all
related to Novak Djokovic.

Top 10 most commented articles between 2am and 6am on Sunday or Monday.

| Event                                                       | category| date | comments
|-------------------------------------------------------------|-------|-------------------------|------|
| Novak Djokovic winning his 10th GS title at US open         | sport | MON 2015-09-14 04:40:00 | 2651 |
| Novak Djokovic losing US open finals                        | sport | MON 2016-09-12 02:19:00 | 1371 |
| Novak Djokovic losing in the first round of the Olympics    | sport | MON 2016-08-08 03:55:00 | 1109 |
| Serbia women's national volleyball team wins silver medal   | sport | SUN 2016-08-21 05:03:00 |  508 |
| Ana Ivanovic beats Serena Williams in AUS Open round of 16  | sport | SUN 2014-01-19 05:01:00 |  335 |
| Riots in Podgorica, Montenegro                              | info  | SUN 2015-10-25 02:25:00 |  235 |
| Serbia men's basketball team advances in the Olympics       | sport | MON 2016-08-15 05:05:00 |  213 |
| Ana Ivanovic out of AUS Open in First round                 | sport | MON 2015-01-19 04:25:00 |  205 |
| Serbia men's basketball team lose in a pre-Olympic friendly | sport | SUN 2016-07-31 03:51:00 |  203 |
| Overview of a day in the Olympics                           | sport | SUN 2016-08-14 05:25:00 |  201 |

### 4) By time comment published

Distribution of comments by the time they are published resembles the one of the times articles are published, i.e. mostly during working days, between 9am and 7pm.

![Comments heatmap by comment time](https://bitbucket.org/mlncvj/b92_comment/raw/7ec8291e8161e716f74f1d7b3cc2181dbcea049d/6.png)

An interesting example of a single user commenting pattern is a user signing himself as "Adriano_Galliani", who left a total of 2274 comments over the analysed period.
It could be concluded that he mostly comments on Champions League games, given that the Wednesday evening time-slot is his "most favourite".

![Comments heatmap by user](https://bitbucket.org/mlncvj/b92_comment/raw/6f2ac66f0dcdd8cadf15a86402c9e27e665618be/7.png)